package com.company.waehrungsrechner_better.adapter;

import com.company.waehrungsrechner_better.ISammelUmrechnung;
import com.company.waehrungsrechner_better.IUmrechnen;
import com.company.waehrungsrechner_better.WR;

public class SammelAdapter implements ISammelUmrechnung {
    private IUmrechnen iUmrechnen;

    public SammelAdapter(IUmrechnen iUmrechnen){
        this.iUmrechnen = iUmrechnen;
    }
    @Override
    public double sammelumrechnen(double[] betraege, String variante) {
        double summe =0;
        for(double betr : betraege){
            summe += iUmrechnen.umrechnen(variante, betr);
        }
        return summe;
    }
}
