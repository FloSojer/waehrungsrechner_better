package com.company.waehrungsrechner_better;

public abstract class WR implements IUmrechnen {
    private WR next;

    public WR(WR wr){
        this.next = wr;
    }

    public WR getNext() {
        return next;
    }

    public double umrechnen(String variante, double betrag){
        if(this.next != null){
           return next.umrechnen(variante, betrag);
        }
        else {return 0;}
    }
}
