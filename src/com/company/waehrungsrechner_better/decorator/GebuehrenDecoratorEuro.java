package com.company.waehrungsrechner_better.decorator;

import com.company.waehrungsrechner_better.IUmrechnen;
import com.company.waehrungsrechner_better.WR;

public class GebuehrenDecoratorEuro extends WR {
    public GebuehrenDecoratorEuro(WR wr) {
        super(wr);
    }
    public double umrechnen(String variante, double betrag){
        return this.getNext().umrechnen(variante, betrag-5);
    }

}
