package com.company.waehrungsrechner_better.decorator;

import com.company.waehrungsrechner_better.WR;

public class GebuehrenDecoratorProz extends WR {
    public GebuehrenDecoratorProz(WR wr) {
        super(wr);
    }
    public double umrechnen(String variante, double betrag){
        return this.getNext().umrechnen(variante, (betrag*(100-5))/100);
    }

}
