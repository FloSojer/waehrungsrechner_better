package com.company.waehrungsrechner_better;

public interface IUmrechnen {
    double umrechnen(String variante, double betrag);
}
