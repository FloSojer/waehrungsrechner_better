package com.company.waehrungsrechner_better;

import com.company.waehrungsrechner_better.adapter.SammelAdapter;
import com.company.waehrungsrechner_better.chainOfResp.EuroToCZK;
import com.company.waehrungsrechner_better.chainOfResp.EuroToDollar;
import com.company.waehrungsrechner_better.chainOfResp.EuroToYen;
import com.company.waehrungsrechner_better.decorator.GebuehrenDecoratorEuro;
import com.company.waehrungsrechner_better.decorator.GebuehrenDecoratorProz;

import java.util.Scanner;

public class Menu {
    public Menu(){
        Scanner scan = new Scanner(System.in);
        int zahl = 0;
        String varia;
        double betr =0, ergeb;
        while(zahl != 6){
            zahl = wasWollenMachen();
            switch (zahl){
                case 1:
                   varia =  variante();
                   betr = betrag();
                    //System.out.println(varia + betr);
                    WR kette = new EuroToDollar(new EuroToYen(new EuroToCZK(null)));
                    ergeb = kette.umrechnen(varia, betr);
                    System.out.println(ergeb);
                    break;
                case 2:
                    int anzahl;
                    varia =  variante();
                    System.out.println("Wie viele Berechnungen wollen Sie machen?");
                    anzahl = scan.nextInt();
                    double[] eingabe = new double[255];
                    for(int i =0; i<anzahl; i++){
                        System.out.println("Bitte geben Sie eine Zahl ein: ");
                        eingabe[i] = scan.nextDouble();
                    }
                    WR ketteSammel = new EuroToDollar(new EuroToYen(new EuroToCZK(null)));

                    ergeb = new SammelAdapter(ketteSammel).sammelumrechnen(eingabe, varia );
                    System.out.println("Das Ergebnis der gesammelten Berrechnung: " + ergeb);
                    break;
                case 3:
                    varia = variante();
                    betr = betrag();
                    WR ketteBuild  = new EuroToDollar(new EuroToYen(new EuroToCZK(null)));
                    EuroToCZK eur2czk= new EuroToCZK.Builder().setNext(ketteBuild).build();
                    ergeb = eur2czk.umrechnen(varia, betr);
                    System.out.println("Mit Builder umgesetzt von "+varia + " ergab: " + ergeb);
                    break;
                case 4:
                    varia = variante();
                    betr = betrag();
                    WR ketteGebEur = new EuroToDollar(new EuroToYen(new EuroToCZK(null)));
                    WR mitGebuehrEur = new GebuehrenDecoratorEuro(ketteGebEur);
                    ergeb = mitGebuehrEur.umrechnen(varia, betr);
                    System.out.println("Mit 5 Euro Abzug berechnet: " + ergeb);
                    break;
                case 5:
                    varia = variante();
                    betr = betrag();
                    WR ketteGebProz = new EuroToDollar(new EuroToYen(new EuroToCZK(null)));
                    WR mitGebuehrProz = new GebuehrenDecoratorProz(ketteGebProz);
                    ergeb = mitGebuehrProz.umrechnen(varia, betr);
                    System.out.println("Mit 5 Prozent Abzug berechnet: " + ergeb);
                    break;
                case 6:
                    System.exit(0);
                default:
                    break;
            }
        }


    }

    public int wasWollenMachen(){
        Scanner scan = new Scanner(System.in);
        int zahl = 0;
        System.out.println("Willkommen im Menue!");
        System.out.println("Was Wollen Sie machen? ");
        System.out.println("1) Eine Umrechnung");
        System.out.println("2) Mehrere Umrechnungen? ");
        System.out.println("3) Umrechnung mit Builder");
        System.out.println("4) Mit 5 Euro Abzug umrechnen");
        System.out.println("5) Mit 5 Prozent Abzug: ");
        System.out.println("6) Prgramm beenden");
        zahl = scan.nextInt();

        return zahl;
    }

    public String variante(){
        String var= "";
        double choice =0;
        while(var == ""){
            Scanner scan = new Scanner(System.in);
            System.out.println("In Welche währungen moechten Sie Rechnen? ");
            System.out.println("1) DOLLAR ");
            System.out.println("2) YEN");
            System.out.println("3) CZK Krone");
            choice = scan.nextInt();
            if(choice == 1){
                var = "EUR2DOLLAR";
                System.out.println("Euro zu Dollar gesetzt");
            }
            else if(choice == 2){
                var = "EUR2YEN";
                System.out.println("Euro zu yen gesetzt");
            }
            else if(choice == 3){
                var = "EUR2CZK";
                System.out.println("CZK Krone");
            }
            else{var = "";}

        }
        return var;
    }
    public double betrag(){
        Scanner scan = new Scanner(System.in);
        double betr = 0;
        System.out.println("Bitte geben Sie Ihren Betrag ein: ");
        betr = scan.nextDouble();
        return betr;
    }
}
