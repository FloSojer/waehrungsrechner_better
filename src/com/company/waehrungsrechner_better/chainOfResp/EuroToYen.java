package com.company.waehrungsrechner_better.chainOfResp;

import com.company.waehrungsrechner_better.WR;

public class EuroToYen extends WR {
    public EuroToYen(WR wr){
        super(wr);
    }

    public double umrechnen(String variante, double betrag) {
        if(variante.equals("EUR2YEN")){
            return betrag*128.98; //stand 23.1.2022
        }
        else {
            return super.umrechnen(variante,betrag);
        }
    }
}
