package com.company.waehrungsrechner_better.chainOfResp;

import com.company.waehrungsrechner_better.WR;

public class EuroToDollar extends WR {
    public EuroToDollar(WR wr){
        super(wr);
    }

    public double umrechnen(String variante, double betrag) {
        if(variante.equals("EUR2DOLLAR")){
            return betrag*1.13; //stand 23.1.2022
        }
        else {
            return super.umrechnen(variante,betrag);
        }
    }

    public static class Builder{
        private WR wr;

        public Builder(){}

        public Builder setNext(WR next){
            this.wr = next;
            return this;
        }
        public EuroToDollar build(){
            return new EuroToDollar(this.wr);
        }
    }
}
