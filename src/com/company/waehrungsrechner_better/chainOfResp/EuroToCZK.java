package com.company.waehrungsrechner_better.chainOfResp;

import com.company.waehrungsrechner_better.WR;

public class EuroToCZK extends WR {
    public EuroToCZK(WR wr){
        super(wr);
    }

    public double umrechnen(String variante, double betrag) {
        if(variante.equals("EUR2CZK")){
            return betrag*24.41; //stand 23.1.2022
        }
        else {
            return super.umrechnen(variante,betrag);
        }
    }

    public static class Builder{
        private WR wr;

        public Builder(){}

        public Builder setNext(WR next){
            this.wr = next;
            return this;
        }
        public EuroToCZK build(){
            return new EuroToCZK(this.wr);
        }
    }
}
