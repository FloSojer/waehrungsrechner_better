package com.company.waehrungsrechner_better;

public interface ISammelUmrechnung {
    double sammelumrechnen(double[] betraege, String variante);
}
